import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;

public class ArrayVisualizer extends JPanel implements MouseMotionListener, MouseListener {
    private static final String NEWLINE = System.lineSeparator();

    private JPanel imagePanel;
    private JTextArea textArea;
    private boolean isMouseMoveListenerOn = true;

    public ArrayVisualizer() {
        super(new BorderLayout(0,0));

        imagePanel = new JPanel();
        add(imagePanel, BorderLayout.CENTER);

        textArea = new JTextArea();
        textArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(textArea);
        scrollPane.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPane.setPreferredSize(new Dimension(200, 75));
        add(scrollPane, BorderLayout.PAGE_END);

        JButton loadButton = new JButton("Open");
        loadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JFileChooser fileChooser = new JFileChooser();
                if(fileChooser.showOpenDialog(imagePanel) == JFileChooser.APPROVE_OPTION) {
                    File file = fileChooser.getSelectedFile();
                    loadArray(file);
                }
            }
        });
        add(loadButton, BorderLayout.PAGE_START);

        //Register for mouse events
        imagePanel.addMouseMotionListener(this);
        imagePanel.addMouseListener(this);

        setPreferredSize(new Dimension(750, 750));
    }

    private void loadArray(File file) {
        // TODO: wczytać to jako jakiś JSON i potem ładnie wypisywać
    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {
        if(isMouseMoveListenerOn) {
            StringBuilder cellContent = new StringBuilder();
            cellContent.append("[");
            cellContent.append(mouseEvent.getPoint().x);
            cellContent.append(",");
            cellContent.append(mouseEvent.getPoint().y);
            cellContent.append("]: ");
            cellContent.append(NEWLINE);
            textArea.append(cellContent.toString());
        }
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        isMouseMoveListenerOn = !isMouseMoveListenerOn;
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {
    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {
    }
}